# Release Notes
2024-12-07

## Added

## Changed

- oml-659: Upgrade to kamailio 5.8. New Dockerfile multistage improve container img size.

## Fixed

## Removed
